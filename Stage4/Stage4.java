import javax.xml.crypto.dom.DOMCryptoContext;
import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Locale;
import java.util.Scanner;

public class Stage4 {
    public static void main(String [] args) throws IOException {
        if (args.length != 1) {
            System.out.println("Usage: java Stage1 <configurationFile.txt>");
            System.exit(-1);
        }
        Scanner in = new Scanner(new File(args[0]));
        in.useLocale(Locale.ENGLISH);

        //System.out.println("File: " + args[0]);
        Cloud cloud = new Cloud();
        // reading <#_de_cortinas> <#_de_lámparas> <#_controles_cortinas> <#_controles_lámparas>
        int numRollerShades = in.nextInt();
        int numLamps = in.nextInt();  // skip number of roller shades
        int numShadeControls = in.nextInt();
        int numLampsControl = in.nextInt(); // skip number of lamp's controls
        // read <alfa0> <length0> <canal0> … <alfaN_1> <lengthN_1> <canalN_1>

        for(int i=0;i<numRollerShades;i++){
            double alpha = in.nextDouble();
            double maxLength = in.nextDouble();
            int channelRoller = in.nextInt();
            RollerShade rollerShade = new RollerShade(channelRoller, alpha, maxLength,i);
            cloud.addRollerShade(rollerShade);
        }

        for(int i=0;i<numLamps;i++){
            int channelLamp = in.nextInt(); // skip lamp's channels
            Lamp lamp = new Lamp(channelLamp, i);
            cloud.addLamp(lamp);
        }
        // creating just one roller shade's control at <canal0>

        ArrayList<DomoticDeviceControl> rollerShadeControls = new ArrayList<>();
        for(int i=0;i<numShadeControls;i++){
            int channelRoller = in.nextInt();
            rollerShadeControls.add(new DomoticDeviceControl(channelRoller,cloud));
        }

        ArrayList<DomoticDeviceControl> lampControls = new ArrayList<>();
        for(int i=0;i<numLampsControl;i++){
            int channelLamp = in.nextInt();
            lampControls.add(new DomoticDeviceControl(channelLamp,cloud));
        }

        Operator operator = new Operator(rollerShadeControls, lampControls,cloud);

        operator.executeCommands(in, System.out);
    }
}
