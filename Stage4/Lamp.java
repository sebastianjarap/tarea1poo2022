public class Lamp extends DomoticDevice {
    public Lamp (int channel,int id){
        super(id, channel);
        state=LampState.OFF;
        r=255;
        g=255;
        b=255;
    }

    {
        id=nextId++;
    }
    public void changePowerState(){
        if (state==LampState.OFF) {
            state=LampState.ON;
        }else{
            state=LampState.OFF;
        }
    }
    public String getHeader(){
        return String.valueOf( "L" + getId());
    }

    public String toString(){
        if (state==LampState.ON)
            return ""+r+"\t"+g+"\t"+b+"\t";
        else
            return "0\t0\t0"+"\t";
    }
    public enum LampState {
        ON,
        OFF
    }

    public LampState getLampState(){
        return state;
    }

    public void changeToGreen(String action){
        if (action.equals("U")){
            if (g>255){
                g=255;
            }else{
                g++;
            }
        } else if (action.equals("D")){
            if (g>=0){
                g--;
            }else{
                g=0;
            }
        } else {
            System.out.println("Action declined");
        }
    }
    public void changeToRed(String action){
        if (action.equals("U")){
            if (r>255){
                r=255;
            }else{
                r++;
            }
        } else if (action.equals("D")){
            if (r>=0){
                r--;
            }else{
                r=0;
            }
        } else {
            System.out.println("Action declined");
        }
    }
    public void changeToBlue(String action){
        if (action.equals("U")){
            if (b>255){
                b=255;
            }else{
                b++;
            }
        } else if (action.equals("D")){
            if (b>=0){
                b--;
            }else{
                b=0;
            }
        } else {
            System.out.println("Action declined");
        }
    }
    private short r,g,b;
    private LampState state;
    private static int nextId=0;
    private static int id;
    private int i;
}
