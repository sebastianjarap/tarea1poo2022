import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<DomoticDevice>();
        shadeControls = new ArrayList<DomoticDeviceControl>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public void addRollerShade(RollerShade rs){
        rollerShades.add(rs);
    }
    public void addRollerShadeControl(ShadeControl sc){
        shadeControls.add(sc);
    }

    public void advanceTime(double delta){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            rs.advanceTime(delta);
        }
    }
    public void changeLampPowerState(int channel){
        for (DomoticDevice dd: lamps){
            Lamp l =(Lamp)dd;
            if (dd.getChannel()==channel){
                l.changePowerState(); // cambia de estado a off u on
            }
        }
    }
    public void startShadeUp(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.startUp();
            }
        }
    }
    public void startShadeDown(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.startDown();
            }
        }
    }
    public void stopShade(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.stop();
            }
        }
    }
    public String getHeaders(){
        String header = "";
        for (DomoticDevice  rs: rollerShades)
            header += rs.getHeader()+"\t";
        for (DomoticDevice l: lamps)
            header += l.getHeader()+"R\t" + l.getHeader()+"G\t" + l.getHeader()+ "B\t";
        return header;
    }
    public Lamp.LampState getGetLampState(int channel) {
        Lamp.LampState state = null;
        for(DomoticDevice dd: lamps){
            Lamp l = (Lamp) dd;
            if (dd.getChannel() == channel) {
                state = l.getLampState();
            }else;
        }
        return state;
    }
    public void setColorRed(int channel, String action){
        for (DomoticDevice dd: lamps) {
            Lamp ll =(Lamp)dd;
            if(getGetLampState(channel)==Lamp.LampState.ON){
                if (dd.getChannel() == channel) {
                    ll.changeToRed(action);
                }
            }
        }
    }
    public void setColorGreen(int channel, String action){
        for (DomoticDevice dd: lamps) {
            Lamp ll =(Lamp)dd;
            if(getGetLampState(channel)==Lamp.LampState.ON){
                if (dd.getChannel() == channel) {
                    ll.changeToGreen(action);
                }
            }
        }
    }
    public void setColorBlue(int channel, String action){
        for (DomoticDevice dd: lamps) {
            Lamp ll =(Lamp)dd;
            if(getGetLampState(channel)==Lamp.LampState.ON){
                if (dd.getChannel() == channel) {
                    ll.changeToBlue(action);
                }
            }
        }
    }
    public String printData(){
        String r = "";
        for (DomoticDevice rs: rollerShades) {
            r += rs.toString();
        }
        for (DomoticDevice l: lamps) {
            r += l.toString();
        }
        return r;
    }
    private ArrayList<DomoticDevice> lamps;
    private ArrayList<DomoticDevice> rollerShades;
    private ArrayList<DomoticDeviceControl> shadeControls;
}
