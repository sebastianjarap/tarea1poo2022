README

Este tarea tiene como finalidad modelar objetos reales como objetos de software, 
utilizando la programacion orientada a objetos para la creacion de
un sistema domotico al programar y modelar cierto tipo de lamparas y 
de cortinas motorizadas.


Especificaciones

Para un correcto funcionamiento del programa como equipo asumimos las siguentes precisiones:

-Todas las abreviaciones de los parámetro o atributos vienen del idioma inglés.
-El archivo de texto utilizado para ejecutar el programa es pasado como parámetro
en la clase que contiene al método main.
-El estado inicial para las lamparas es apagado y al encenderlas parten con
la mayor de sus intensidades.
-Si el usuario intenta superar la máxima intensidad de las lamparas, o de la misma forma 
reducir la mínima intensidad de estas, se mostrará por pantalla un mensaje diciendo que la 
acción fue rechazada.
-La nube funciona como un intermediario entre los controles y los objetos.
-Del mismo modo que en las lamparas, para el caso de las cortinas si el usuario quiere
estirar o enrollar mas de lo posible el objeto se muestra por pantalla un mensaje diciendo
que la acción fue rechazada.



instrucciones de comilado y ejecución

EL programa puede ser compilado y ejecutado en cada una de sus 4 etapas, obteniendo
así la solucion para cada subconjunto de los requerimientos pedidos. 
Cada etapa es compilada y ejecutada de la misma manera, hay que realizar 
los siguientes comandos al estar dentro de una terminal ubicada en la carpeta donde se 
encuentra este proyecto:
	>Make        #Para compilar el archivo
	>make run    #Para ejecutr el archivo  
	>make clear  #Para Eliminar el archivo ejecutable creado 



trabajo realizado por:
Sebastian andres jara palomino rol:201944515-4
Agustín Ovando Saenz rol: 201930544-1
& Benjamín Andrés Quiroz Aranguiz rol:201904509

estudiantes de ingeniería civil telemática, UTFSM