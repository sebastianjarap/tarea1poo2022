public class LampControl {
    public LampControl(int channel, Cloud c){
        this.cloud = c;
        this.channel = channel;
    }

    public void pressPower(){
        cloud.changeLampPowerState(channel);;
    }

    public void setColorGreen(int channel, String action){
        if(cloud.getState()==LampState.ON){
            cloud.getLampAtChannel(channel).changeToGreen(action);
        }else ;
    }
    public void setColorRed(int channel, String action){
        if(cloud.getState()==LampState.ON){
            cloud.getLampAtChannel(channel).changeToRed(action);
        }else ;
    }
    public void setColorBlue(int channel, String action){
        if(cloud.getState()==LampState.ON){
            cloud.getLampAtChannel(channel).changeToBlue(action);
        }else ;
    }

    private Cloud cloud;
    private int channel;
}
