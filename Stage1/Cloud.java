import java.util.ArrayList;

public class Cloud {
    public Cloud(){
        lamps = new ArrayList<Lamp>();
    }

    public void addLamp(Lamp l){
        lamps.add(l);
    }


    public Lamp getLampAtChannel( int channel){
        for (int i=0; i<lamps.size();i++){
            if(lamps.get(i).getChannel()==channel){
                return lamps.get(i);
            }
        }
        return null;
    }

    public void changeLampPowerState(int channel){
        for (Lamp l: lamps){
            if (l.getChannel()==channel){
                l.changePowerState(); // cambia de estado a off u on
            }
        }
    }

    public String getHeaders(){
        String header = "L";
        for (Lamp l: lamps)
            header += l.getHeader();
        return header;
    }
    public static boolean getArrayList(){
        for (int i=0; i<lamps.size();i++) {
            System.out.println(lamps.get(i));
        }
        return false;
    }

    public LampState getState() {
        return lamps.get(0).getState();
    }

    public String getRgb(int channel){
        return getLampAtChannel(channel).toString();
    }

    private static ArrayList<Lamp> lamps; // getting ready for next stages
    private LampState state;
}
