import java.io.PrintStream;
import java.util.Scanner;

public class Operator {
    public Operator(LampControl lc, Cloud c){
        lampControl=lc;
        cloud=c;
    }
    public void executeCommands(Scanner in, PrintStream out){
        System.out.println(String.format(("Time\t"+"State"+cloud.getHeaders()+"R"+cloud.getHeaders()+"\tG"+cloud.getHeaders()+"\tB"+cloud.getHeaders())));
        while(in.hasNext()) {
            time = in.nextInt();
            string = in.next();
            channel = in.nextInt();
            a1 = in.next();
            if (!string.equals("L")) {
                out.println("Unexpected device:" + string);
                continue;
            } else {
                if (cloud.getState() == LampState.OFF && a1.equals("F")) {
                    ;
                } else if (cloud.getState() == LampState.OFF && a1.equals("N")) {
                    lampControl.pressPower();
                } else if (cloud.getState() == LampState.ON && a1.equals("N")) {
                    ;
                } else if (cloud.getState() == LampState.ON && a1.equals("F")) {
                    lampControl.pressPower();
                } else if (a1.equals("R")) {
                    a2 = in.next();
                    lampControl.setColorRed(channel, a2);
                } else if (a1.equals("B")) {
                    a2 = in.next();
                    lampControl.setColorBlue(channel, a2);
                } else if (a1.equals("G")) {
                    a2 = in.next();
                    lampControl.setColorGreen(channel, a2);
                } else {
                    System.out.println("Action not found");
                }
                System.out.println(time + "\t" + cloud.getState() + "\t" + cloud.getRgb(channel));
            }
        }
    }
    private int time;
    private Lamp lamp;
    private LampControl lampControl;
    private Cloud cloud;
    private String string;
    private int channel;
    private String a1;
    private String a2;
}