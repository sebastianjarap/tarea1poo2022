public class Lamp extends DomoticDevice {
    public Lamp (int channel){
        super(id, channel);
        state=LampState.OFF;
        r=255;
        g=255;
        b=255;
    }

    {
        id=nextId++;
    }

    public void changePowerState(){
        // ???
    }
    public String getHeader(){
        return String.valueOf(id + " ");
    }
    public String toString(){
        if (state==LampState.ON)
            return ""+r+"\t"+g+"\t"+b;
        else
            return "0\t0\t0";
    }
    private enum LampState {
        ON,
        OFF
    }
    public void startUp(){

    }
    public void startDown(){

    }
    public void stop(){

    }

    private short r,g,b;
    private LampState state;
    private static int nextId=0;
    private static int id;

}
