import java.io.PrintStream;
import java.util.Scanner;

public class Operator {
    public Operator(ShadeControl sc, Cloud c){
        this.cloud = c;
        this.rsControl = sc;
    }
    public void addShadeControl(ShadeControl sc){
        cloud.addRollerShadeControl(sc);
    }

    public void executeCommands(Scanner in, PrintStream out){
        out.println("Time\t" + cloud.getHeaders());
        while(in.hasNextInt()){
            int commandTime=in.nextInt();
            //System.out.println(commandTime);
            while (time < commandTime) {
                System.out.println(String.format("%.1f",time) +"\t \t"+cloud.getState());
                cloud.advanceTime(delta);
                time+=delta;
            }
            String device=in.next();
            if (!device.equals("C")) {
                out.println("Unexpected device: " + device);
                System.exit(-1);
            }
            int channel = in.nextInt();
            command=in.next();
            if (channel == rsControl.getChannel()) {
                switch (command.charAt(0)) {
                    case 'D': //??
                        cloud.startShadeDown(channel);
                        break;
                    case 'U':
                        cloud.startShadeUp(channel);
                        break;
                    case 'S':
                        cloud.stopShade(channel);
                        break;
                    default: out.println("Unexpected command: " + command);
                }
            }
        }
        System.out.println(String.format("%.1f",time) +"\t \t"+cloud.getState());
    }
    private double time=0;
    private ShadeControl rsControl;
    private Cloud cloud;
    private final double delta=0.1;
    private String command;
}
