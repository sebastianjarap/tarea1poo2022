import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<RollerShade>();
        shadeControls = new ArrayList<DomoticDeviceControl>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }

    public void addRollerShade(RollerShade rs){
        rollerShades.add(rs);
    }
    public void addRollerShadeControl(ShadeControl sc){
        shadeControls.add(sc);
    }

    public void advanceTime(double delta){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            rs.advanceTime(delta);
        }
    }

    private DomoticDevice getDomoticDeviceAtChannel( ArrayList<DomoticDevice> devices, int channel){ //arreglar
        for (int i=0; i<devices.size();i++){
            if(devices.get(i).getChannel()==channel){
                return devices.get(i);
            }
        }
        return null;
    }
    public void changeLampPowerState(int channel){
        // ???
    }
    public void startShadeUp(int channel){
        for (RollerShade d: rollerShades) {
            if (d.getChannel() == channel) {
                d.startUp();
            }
        }
    }

    public void startShadeDown(int channel){
        for (RollerShade d: rollerShades) {
            if (d.getChannel() == channel) {
                d.startDown();
            }
        }
    }
    public void stopShade(int channel){
        for (RollerShade d: rollerShades) {
            if (d.getChannel() == channel) {
                d.stop();
            }
        }
    }
    public String getHeaders(){
        String header = "";
        for (DomoticDevice  rs: rollerShades)
            header += rs.getHeader()+"\t";
        for (DomoticDevice l: lamps)
            header += l.getHeader()+"\t";
        return header;
    }
    public String getState(){
        if (rollerShades.get(0).getGetMotorState() == RollerShade.Motor.MotorState.DOWNWARD){
            return "-100";
        }else if((rollerShades.get(0).getGetMotorState()== RollerShade.Motor.MotorState.UPWARD)){
            return "100";
        }else{
            return "0";
        }
    }

    private ArrayList<DomoticDevice> lamps;
    private ArrayList<RollerShade> rollerShades;
    private ArrayList<DomoticDeviceControl> shadeControls;
}
