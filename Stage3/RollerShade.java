public class RollerShade extends DomoticDevice {

    public RollerShade (int channel, double alpha, double length, int id) {
        super(id, channel);
        motor = new Motor(alpha);
        this.maxShadeLength = length;// largo total
    }
    public void startUp(){
        motor.turnUp();
    }
    public void startDown(){
       motor.turnDown();
    }
    public void stop(){
        motor.stop();
    }
    public void advanceTime(double delta){
        motor.advanceTime(delta);
    }
    public String getHeader(){
        String s = "RS" + getId();
        return s;
    }
    public String toString(){
        String s = String.valueOf(Math.round(length/maxShadeLength*100) +"%\t");
        return s;
    }

    public Motor.MotorState getGetMotorState(){
        return motor.state;
    }

    protected class Motor {  //nested class, Motor is only used within RollerShade.
        public Motor (double a){
            alpha = a;
            state = MotorState.STOPPED;
        }
        public enum MotorState {
            UPWARD,
            STOPPED,
            DOWNWARD
        }
        public void turnUp(){
            state = MotorState.UPWARD;
        }
        public void turnDown(){
            state = MotorState.DOWNWARD;
        }
        public void stop(){
            state = MotorState.STOPPED;
        }
        public void advanceTime(double delta){
            double increment = alpha*delta*RADIUS;
            switch (state) {
                case STOPPED:
                    break;
                case DOWNWARD:
                    if (length + increment <= maxShadeLength){
                        length += increment;
                    }else {
                        length = maxShadeLength;
                    }
                    break;
                case UPWARD:
                    if (length - increment <= 0){
                        length = 0;
                    }else {
                        length -= increment;
                    }
                    break;
            }
        }
        /*public MotorState getMotorState() {
            return state;
        }*/

        private double alpha;
        private MotorState state;
        private static double RADIUS=0.04;
    }

    private Motor motor;
    private double length = 0; //largo actual
    private final double maxShadeLength; //largo maximo del roller
    private static int nextId;
    private static int id;
    private Motor state;
}
