public class DomoticDeviceControl {
    public DomoticDeviceControl(int ch, Cloud c){
        channel = ch;
        cloud = c;
    }
    public int getChannel() {
        return channel;
    }
    public void pressPower(int channel){
        cloud.changeLampPowerState(channel);
    }
    public void StarUp(){
        cloud.startShadeUp(channel);
    }
    public void StarDown(){
        cloud.startShadeDown(channel);
    }
    public void Stop(){
        cloud.stopShade(channel);
    }
    protected Cloud cloud;
    protected int channel;
}
