import java.util.ArrayList;

public class Cloud {
    public Cloud() {
        lamps = new ArrayList<DomoticDevice>();
        rollerShades = new ArrayList<DomoticDevice>();
        shadeControls = new ArrayList<DomoticDeviceControl>();
    }
    public void addLamp(Lamp l){
        lamps.add(l);
    }
    public void addRollerShade(RollerShade rs){
        rollerShades.add(rs);
    }
    public void addRollerShadeControl(ShadeControl sc){
        shadeControls.add(sc);
    }

    public void advanceTime(double delta){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            rs.advanceTime(delta);
        }
    }

    private DomoticDevice getDomoticDeviceAtChannel( ArrayList<DomoticDevice> devices, int channel){ //arreglar
        for (int i=0; i<devices.size();i++){
            if(devices.get(i).getChannel()==channel){
                return devices.get(i);
            }
        }
        return null;
    }
    public void changeLampPowerState(int channel){
        for (DomoticDevice dd: lamps){
            Lamp l =(Lamp)dd;
            if (dd.getChannel()==channel){
                l.changePowerState(); // cambia de estado a off u on
            }
        }
    }
    public void startShadeUp(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.startUp();
            }
        }
    }
    public void startShadeDown(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.startDown();
            }
        }
    }
    public void stopShade(int channel){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (dd.getChannel() == channel) {
                rs.stop();
            }
        }
    }
    public String getHeaders(){
        String header = "";
        for (DomoticDevice  rs: rollerShades)
            header += rs.getHeader()+"\t";
        for (DomoticDevice l: lamps)
            header += l.getHeader()+"R\t" + l.getHeader()+"G\t" + l.getHeader()+ "B\t";
        return header;
    }

    public Lamp.LampState getGetLampState(int channel) {
        Lamp.LampState state = null;
        for(DomoticDevice dd: lamps){
            Lamp l = (Lamp) dd;
            if (dd.getChannel() == channel) {
                state = l.getLampState();
            }else;
        }
        return state;
    }

    public Lamp getLampAtChannel( int channel){
        for (int i=0; i<lamps.size();i++){
            if(lamps.get(i).getChannel()==channel){
                return (Lamp) lamps.get(i);
            }
        }
        return null;
    }

    public void setColorGreen(int channel, String action){
        for (DomoticDevice dd: lamps) {
            Lamp ll =(Lamp)dd;
            if(getGetLampState(channel)==Lamp.LampState.ON){
                if (dd.getChannel() == channel) {
                    ll.changeToGreen(action);
                }
            }
        }

        /*

        if(getGetLampState()==Lamp.LampState.ON){
            getLampAtChannel(channel).changeToGreen(action);
        }else;


        if(getGetLampState()==Lamp.LampState.ON){
            for(DomoticDevice dd: lamps) {
                Lamp ll = (Lamp) dd;
                ll.changeToGreen(action);
            }
        }else;

         */
    }

    public String getStateRoller(){
        for (DomoticDevice dd: rollerShades) {
            RollerShade rs =(RollerShade)dd;
            if (rs.getGetMotorState() == RollerShade.Motor.MotorState.DOWNWARD){
                return "-1";
            }else if((rs.getGetMotorState()== RollerShade.Motor.MotorState.UPWARD)){
                return "1";
            }else{
                return "0";
            }
        }
        return null;
    }
    public String printData(){
        String r = "";
        for (DomoticDevice rs: rollerShades) {
            r += rs.toString();
        }
        for (DomoticDevice l: lamps) {
            r += l.toString();
        }
        return r;
    }

    private ArrayList<DomoticDevice> lamps;
    private ArrayList<DomoticDevice> rollerShades;
    private ArrayList<DomoticDeviceControl> shadeControls;
}
