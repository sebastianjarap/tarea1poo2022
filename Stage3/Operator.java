import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Scanner;
import java.util.concurrent.atomic.AtomicBoolean;

public class Operator {
    public Operator(ArrayList<DomoticDeviceControl> sc, ArrayList<DomoticDeviceControl> lc, Cloud c){
        this.shadeControls = sc;
        this.lampControls = lc;
        this.cloud = c;
    }

    public void addShadeControl(ShadeControl sc){
        cloud.addRollerShadeControl(sc);
    }

    public void executeCommands(Scanner in, PrintStream out){
        out.println("Time\t" + cloud.getHeaders());
        while(in.hasNextInt()){
            int commandTime=in.nextInt();
            //System.out.println(commandTime);
            while (time < commandTime) {
                System.out.println(String.format("%.1f",time) +"\t" + cloud.printData());
                cloud.advanceTime(delta);
                time+=delta;
            }
            String device=in.next();
            if (device.equals("C")){
                int channel = in.nextInt();
                command=in.next();
                for (DomoticDeviceControl ddc: shadeControls){
                    if (channel == ddc.getChannel()) {
                        switch (command.charAt(0)) {
                            case 'D': //??
                                ddc.StarDown();
                                break;
                            case 'U':
                                ddc.StarUp();
                                break;
                            case 'S':
                                ddc.Stop();
                                break;
                            default: out.println("Unexpected command: " + command);
                        }
                    }
                }
            }else if(device.equals("L")){
                int channel = in.nextInt();
                a1 = in.next();
                if (cloud.getGetLampState(channel) == Lamp.LampState.OFF && a1.equals("F")) {
                    continue;
                }else if (cloud.getGetLampState(channel) == Lamp.LampState.OFF && a1.equals("N")) {
                    cloud.changeLampPowerState(channel);
                    continue;
                } else if (cloud.getGetLampState(channel) == Lamp.LampState.ON && a1.equals("N")) {
                    continue;
                } else if (cloud.getGetLampState(channel) == Lamp.LampState.ON && a1.equals("F")) {
                    cloud.changeLampPowerState(channel);
                    continue;
                }else if (a1.equals("R")) {
                    a2 = in.next();
                    cloud.setColorGreen(channel, a2);
                    continue;
                } else if (a1.equals("B")) {
                    a2 = in.next();
                    cloud.setColorGreen(channel, a2);
                    continue;
                } else if (a1.equals("G")) {
                    a2 = in.next();
                    cloud.setColorGreen(channel, a2);
                    continue;
                }
                in.next();
            }else {
                System.out.println("Unexpected device:" + device);
                in.nextLine();
            }
        }
        System.out.println(String.format("%.1f",time) +"\t" + cloud.printData());
    }
    private double time=0;
    private ShadeControl rsControl;

    private LampControl lampControl;
    private DomoticDeviceControl DomoticControl;

    private ArrayList<DomoticDeviceControl> shadeControls = null;
    private ArrayList<DomoticDeviceControl> lampControls = null;
    private Cloud cloud;
    private final double delta=0.1;
    private String command;

    String a1,a2;
}
